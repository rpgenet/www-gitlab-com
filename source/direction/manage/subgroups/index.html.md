---
layout: markdown_page
title: "Category Direction - Subgroups"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [groups](https://gitlab.com/groups/gitlab-org/-/epics?label_name=groups) |

## Groups

Groups are a fundamental building block (a [small primitive](https://about.gitlab.com/handbook/product/#prefer-small-primitives)) in GitLab for project organization and managing access to these resources at scale. Like folders in a filetree, they organize like resources into one place - whether that’s a handful of projects all belonging to a backend team, or organizing a group of user researchers into one place to make access control and communication easier.

Since groups cover multiple projects, we also scope a number of features at the group level like  epics, contribution analytics, and the security dashboard. 

In 2019, our goal is to improve groups with improvements to access control, enhancements to the user experience, and by introducing a first iteration at a type of group specific for teams of people.

## Target audience and experience

TBD

## Maturity

As groups are a GitLab-specific concept, it's considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions.

## How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/587).
1. Find issues in this category [accepting merge requests](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=groups).

## What's next & why

### Current

#### Better access control
Currently, adding new members to groups is largely permissive. Since groups tend to organize many important instance resources, we want to give administrators tools to restrict access to group resources and ensure that group members are in compliance.

We’re also improving on group authentication strategies that enterprises need to be successful (especially important for GitLab.com). Please see the ~authentication category epic for more detail.
* Related epics: https://gitlab.com/groups/gitlab-org/-/epics/84

### Next

#### Optional membership inheritance for subgroups
Currently, subgroups always inherit members from parent groups. Not being able to selectively remove subgroup membership from a top-level group makes groups challenging to organize, especially on GitLab.com - where most customers are represented by a single top-level group. These groups should have the ability to configure sub-groups with whatever visibility settings that are required.
* Related epics: https://gitlab.com/groups/gitlab-org/-/epics/588

## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD
