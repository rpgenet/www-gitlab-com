---
layout: markdown_page
title: "Category Direction - User Entity and Behavioral Analytics"
---

## Description
User Entity and Behavior Analytics (UEBA) is a way to identify attacks and high risk behaviors by correlating different data sources and observing behavioral patterns. This allows attacks to be observed that are only apparent with context from multiple sources, rather than just a single event in isolation.

### Goal

Our goal is to provide UEBA capabilities for both your applications, as well as GitLab itself. We will help proactively identify malicious traffic, potentially compromised user accounts or infrastructure components, anomalous use of the GitLab platform, and various high-risk behaviors so that actionable remediation steps are possible.

Because UEBA solutions are often broad and require complex data setups, we will instead take a "build up, then out" approach. This way, we can provide immediate value in small increments. We will add new anomaly detection capabilities as distinct features. These can be configured and improved incrementally, independent of other detection capabilities. Over time, these features will converge, joining underlying analytics and detection capabilities to provide more horizontal insights and detection capabilities.

Our UEBA goals align with the goals of our [Defend Guiding Principles](https://about.gitlab.com/direction/defend/#guiding-principles) in that we strive to offer these capabilities "batteries included" with minimal to no configuration for initial usage. We will default to presenting actionable insights but will leave the decision to block up to you unless specifically configured otherewise.

Another UEBA goal is that GitLab will feed back our results to other stages, so any necessary actions can be taken there, in addition to defending the app itself. An example could include creating an issue about access controls for a specific region of the app that is being exploited by malicious users.

### Roadmap
Forthcoming

## What's Next & Why
In the spirit of MVC, we will look to productize an existing capabilities developed by our own security team. We will [incorporate a machine learning (ML) model](https://gitlab.com/gitlab-org/gitlab/issues/201576) that can identify runner abuse, specifically when they are used for cryptomining.  This will help lay the foundation for future behavioral models by starting to define how they are configured by end users as well as the underlying deployment and upgrade model.

We will also [start investigating](https://gitlab.com/groups/gitlab-org/-/epics/1786) open source UEBA platforms to see if there is a suitable option to form the backbone of our horizontally-focused security data analytics and visibility capabilities.

## Competitive Landscape
Many of today's UEBA products are focused on the desktop environment, using deployed agents to gather data and monitor behavior. Most solutions are run as a physical appliance or in an on-premises data center. Virtual machines for cloud deployment are in the minority; cloud-native UEBA is even less common.

Additionally, there continues to be a convergence of UEBA features with adjacent products such as SIEMs or acquisition of standalone vendors by larger security companies. According to Gartner, "by 2022, 95% of all UEBA deployments will be 'as a feature' of broader security platforms." This indicates we are potentially already past the inflection point of standalone UEBA systems being a viable future option&mdash;especially given the minority of cloud-native offerings today.

eSecurity Planet has a [UEBA buying guide](https://www.esecurityplanet.com/products/top-ueba-vendors.html) which has a list of competitive offerings.

## Analyst Landscape
*  Gartner has a [Market Guide for UEBA](https://www.gartner.com/reviews/market/user-and-entity-behavior-analytics)
*  Forrester covers UEBA functionality as part of its Wave report for [Security Analytics Platforms](https://www.forrester.com/report/The+Forrester+Wave+Security+Analytics+Platforms+Q3+2018/-/E-RES141154)

## Top Customer Success/Sales Issue(s)

There is no feature available for this category.

## Top Customer Issue(s)

The category is very new, so we still need to engage customers and get feedback about their interests and priorities in this area.

## Top Vision Item(s)

[Item 1](https://gitlab.com/groups/gitlab-org/-/epics/2545)
