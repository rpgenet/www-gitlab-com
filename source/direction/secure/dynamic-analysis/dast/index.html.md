---
layout: markdown_page
title: "Category Direction - Dynamic Application Security Testing"
---

- TOC
{:toc}

## Description

### Overview

Dynamic application security testing (DAST) is a process of testing an application or software product in an operating state. 

When application has been deployed and started, DAST connects to the published service via its standard web port and performs a scan of the entire application. It can enumerate pages and verify if well-known attack techniques, like cross-site scripts or SQL injections are possible.

DAST doesn't need to be very language specific, because the tool emulates a web client interacting with the application.

### Goal

Our goal is to provide DAST as part of the standard development process. This means that DAST is executed every time a new commit is pushed to a branch. We also include DAST as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Since DAST requires a running application, we can provide results for feature branches leveraging [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/), temporary environments that run the modified version of the application.

DAST results can be consumed in the merge request, where only new vulnerabilities, introduced by the new code, are shown. A full report is available in the pipeline details page.

DAST results will also be part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status.

We also want to ensure that the production environment is always secure, by running DAST on the deployed app even if there is no change in the code. This can be done using the [Security Control Panel](https://gitlab.com/groups/gitlab-org/-/epics/307).

### Roadmap

- First MVC (already shipped): https://docs.gitlab.com/ee/user/application_security/dast/
- Dynamic Application Security Testing (DAST) improvements: https://gitlab.com/groups/gitlab-org/-/epics/300

## What's Next & Why

We want to make DAST suitable for multiple environments (including production), and run it with proper settings depending on the case, without slowing down the process.

The next MVC is to support different types of DAST scan: https://gitlab.com/gitlab-org/gitlab-ee/issues/8577.

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/528)

## Competitive Landscape

- [CA Veracode](https://www.veracode.com/products/dynamic-analysis-dast)
- [Microfocus Fortify WebInspect](https://software.microfocus.com/en-us/products/webinspect-dynamic-analysis-dast/overview)
- [IBM AppScan](https://www.ibm.com/security/application-security/appscan)
- [Rapid7 AppSpider](https://www.rapid7.com/products/appspider)
- [Detectify](https://detectify.com/)

We have the advantage to provide testing results before the app is deployed into the production environment, by using Review Apps. This means that we can provide DAST results for every single commit.

Anyway, we can [provide support for custom flows](https://gitlab.com/gitlab-org/gitlab-ee/issues/8576) and provide [DAST results for the master branch in Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/5503) to better align with other existing tools.

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. Since this is a relatively new scope for us, we must aim at being included in the next researches.

We can get valuable feedback from analysts, and use it to drive our vision.

- [Gartner](https://www.gartner.com/reviews/market/application-security-testing)
- [Forrester](https://www.forrester.com/report/Predictions+2019+Cybersecurity/-/E-RES144821)

We can also make them able to easily compare our tool with other existing solutions by supporting the [OWASP WebGoat project](https://gitlab.com/gitlab-org/gitlab-ee/issues/9819).

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=analysts&label_name[]=dast)

## Top Customer Success/Sales Issue(s)

- [Top Issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/7508)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=customer&label_name[]=dast)

## Top user issue(s)

- [Top Issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/7508)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=popularity&label_name[]=dast)

## Top internal customer issue(s)

- [Top Issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/8577)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=internal%20customer&label_name[]=dast)

## Top Vision Item(s)

- [Vision Item 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/8483)
- [Vision Item 2](https://gitlab.com/gitlab-org/gitlab-ee/issues/5503)
- [Vision Item 3](https://gitlab.com/groups/gitlab-org/-/epics/307)
