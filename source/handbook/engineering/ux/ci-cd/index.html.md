---
layout: handbook-page-toc
title: "CI/CD UX"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview

GitLab CI/CD is a powerful tool built into GitLab that allows our customers to apply all the continuous methods (Continuous Integration, Delivery, and Deployment) to their software with no third-party application or integration needed.

The goal of the CI/CD UX team is to provide a seamless experience and clear guidance to empower users of all knowledge levels to ship their code as fast as possible without interruptions. 
Our design mission is bringing to the forefront simple, clean ways to make GitLab be the tool of choice for deploying where, when, and how users want to.

### Team

See all [CI/CD UX Team members](https://about.gitlab.com/company/team/?department=ci-cd-ux-team) and open vacancies.

### Our strategy

Our strategy is all about making sure that even complex delivery flows become an effortless part of everyone's primary way of working. We work closely with Engineering, Product Management, User Research, Technical Writing, and Product Marketing to make sure we have all the support in uncovering user needs and work to solve them together.  

Visit [CI/CD Product Section Direction](/direction/cicd/) to read about the product strategy. 

### Customer

Our solutions are meant to help organisations scale efficiently and effectively by automating software delivery processes and orchestrating deployments to go production at a low cost. We are focusing on these [roles and personas](/handbook/marketing/product-marketing/roles-personas/#user-personas) to create the best user experience possible.

### UX pages

* Verify: Coming soon!
* [Release UX](/handbook/engineering/ux/stage-group-ux-strategy/release/)
* [Package UX](/handbook/engineering/ux/stage-group-ux-strategy/package/) 

### How we work 

#### Quarterly OKRs

[OKRs](https://en.wikipedia.org/wiki/OKR) stand for Objectives and Key Results and we manage them as quarterly goals. They lay out our plan to execute our strategy and help make sure our goals and how to achieve them are clearly defined and aligned throughout the organization. 

Learn more about [CI/CD UX Team OKRs](https://gitlab.com/gitlab-org/gitlab-design/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX%20CICD%20Team&label_name[]=OKR).

#### User research

Our goal is to stay connected with GitLab users all around the world and gather insight into their behaviors, motivations, and goals when using GitLab. 

* [Verify UX research insights](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=devops%3A%3Averify)
* [Release UX research insights](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=devops%3A%3Arelease) 
* [Package UX research insights](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=devops%3A%3Apackage)

Read more about how we do [UX Research at GitLab](/handbook/engineering/ux/ux-research/).

#### Customer journeys

We see customer journeys as stories about understanding our users, how they behave, and what we can do to improve their trip. We use [UX Scorecards](/handbook/engineering/ux/ux-scorecards/) to perform this analysis and evaluate user experience.

See all [CI/CD UX Scorecards](https://gitlab.com/gitlab-org/gitlab/issues/197959#note_276290767).

#### Understanding business objectives

We work closely with Product Management to understand business goals by collaboratively answering product foundational questions:

* [Release Product Foundations Document](https://gitlab.com/gitlab-org/gitlab-design/issues/392) (ongoing)
* [Verify Product Foundations Document](https://gitlab.com/gitlab-org/gitlab-design/issues/334) (ongoing)

### Follow our work

* [By-weekly CI/CD UX team meetings](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpap0GkV0SSuGnPhCM8jrAv) 
* [CI/CD UX team Design Reviews](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpnb8RDztlfpryAYip1OMwb)


