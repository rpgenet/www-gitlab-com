---
layout: handbook-page-toc
title: "Competencies"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

GitLab has competencies as a common framework to learn things.
The competencies include both general and role specific compentencies.
Competences are useful to have a [Single Source of Truth (SSoT)](https://docs.gitlab.com/ee/development/documentation/styleguide.html#why-a-single-source-of-truth) for: 

## Principles

1. Re-use the same materials for different audiences by having them on a competency page.
1. Work handbook first so everyone can contribtue (link to mission and embed video https://about.gitlab.com/handbook/sales/field-operations/sales-enablement/#handbook-first-approach-to-gitlab-learning-and-development-materials )
1. Accessible to everyone in the world, including doing the test and receiving the certification (via Google forms and Zapier)

## Usage

1.  [Job family requirements](/handbook/hiring/job-families/#format)
1.  [Interview scoring](/handbook/hiring/recruiting-framework/hiring-manager/#step-12hm-complete-feedback-in-greenhousenext-steps)
1.  [Promotion criteria](/handbook/people-group/promotions-transfers/)
1.  [9 box assessments](https://www.predictivesuccess.com/blog/9-box/)
1.  [Performance/Potential criteria](https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix)
1.  [Succession planning](https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning/#succession-planning)
1.  [Learning and development](/handbook/people-group/learning-and-development/)
1.  [PDPs/PIPs](/handbook/underperformance/)
1.  [Career development](/handbook/people-group/learning-and-development/career-development/)
1.  [360 reviews](SHADOWS)
1.  [Manager toolkit](https://gitlab.com/gitlab-com/people-group/people-group-senior-leader-priorities/issues/2)
1.  [Sales training](https://about.gitlab.com/handbook/sales/training/)
1.  [Sales enablement sessions](https://about.gitlab.com/handbook/sales/training/sales-enablement-sessions/)
1.  [Field enablement](https://about.gitlab.com/handbook/sales/field-operations/sales-enablement/)
1.  [GitLab Training tracks](https://about.gitlab.com/training/)
1.  [GitLab University](https://docs.gitlab.com/ee/university/)
1.  [Customer Success Skills Exchange Sessions](https://about.gitlab.com/handbook/sales/training/customer-success-skills-exchange/)
1.  [Professional services offerings](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/offerings/)
1.  [Onboarding](https://about.gitlab.com/handbook/general-onboarding/) both general and department specific
1.  [Reseller onboarding](https://about.gitlab.com/handbook/resellers/onboarding/)

## Structure

1. Content is in our handbook (with embedded videos and pictures)
1. Test in in Google Forms (via Zapier [you get a certification](/handbook/people-group/learning-and-development/certifications/#how-to-create-a-certification))
1. The [leadership forum](https://about.gitlab.com/handbook/people-group/learning-and-development/leadership-forum/) is organized by L&D
1. Maybe we can also do about 5 questions (with example of a good answer/level) per level

## List

1. Results
1. Iteration
1. Transparency
1. Collaboration
1. Diversity and inclusion
1. Efficiency
1. Manager of 1
1. Working async
1. Well written artifacts
1. Single Source of Truth
1. Producing video
1. Handbook first
1. Install GitLab
1. GitLab administration
1. ROI calculation

