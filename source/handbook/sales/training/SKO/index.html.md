---
layout: handbook-page-toc
title: "Sales Kick Off 2020"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SKO Overview
Motivate the team, outline strategy, and celebrate wins. This event sets the tone tone for the next 12 months and is especially critical for those newer to the team to educate them on how we work and to reenergize those that have been here longer and to get everyone fired up to hit their targets.
### Goals
* boot morale
* build up the teams and relationships in person
* have entire org and supporting groups understand our roadmap, priorities and how to be successful at GitLab in the next year. 

After participating in the FY21 GitLab SKO, sales team members will…
 * Be energized about GitLab’s vision & strategy and the opportunities that lie ahead (for GitLab, our customers, and for them personally)
 * Return home with a clear understanding of what’s needed to seize the opportunity and how to plan & execute their FY21 business

### High-level agenda 
* Pre work: Embrace remote → learn about product strategy/roadmap prior to SKO
* Day 1 : Travel Day plus evening Welcome Reception
* Day 2: Keynote Q&As, speakers, and evening Awards Dinner
* Day 3: Role-based and Team-based training
* Day 4: Regional QBRs
* Day 5: Travel Day to return home
All subject to changebased on feedback from FY21 event. 

### Motivational theme
**Level Up**- It works across nationalities / languages and has lots of ways we can tie it into the content. All about leveling up the business. 

# SKO Planning
* Phases:
  * Plan (pre event)
    * Scope
    * Design
    * Develop
  * Execute (during event)
    * Ongoing
    * Onsite
  * Reinforce / Monitor (post event)
    * Event
    * Internal
    * External

# **Sales Kick Off 2022**
Aiming for Feb 2021, details coming soon. Likely in NORAM week of Feb 8th.
[WIP landing page](https://about.gitlab.com/events/sko21/)

# **Sales Kick Off 2021**

In February 2020, the entire sales organization and their supporting teams gathered in Vancouver, BC Canada for the inaugural GitLab Sales Kick Off. This page includes slide decks, videos, and pictures from the event.

**Day 1 Sessions**
1.  [Welcome & FY21 Overall GitLab Strategy & Vision](https://drive.google.com/open?id=1mvqXfa3vIQec8mhBSzVB5gZDC7LJwdMhjHev0nkVj9g)
     * Video: *coming soon*
1.  [Product Strategy & Vision with Q&A](https://drive.google.com/open?id=1DfsjznCwqBz9MpueftriC1QS81QHuXcdAACqtdcFPU0)
     * Video: *coming soon*
1.  [FY21 Sales & Marketing Strategy & Vision with Q&A](https://drive.google.com/open?id=1bq_bxM07PTNF3rfAbFB_Fia7U8CszfLTEJUrF-bfilI)
     * Video: *coming soon*
1.  CRO Staff Level Up Panel Discussion (no slides)
     * Video: *coming soon*
1.  [Leveling Up with Partners at GitLab](https://drive.google.com/open?id=12j219pElrox1hUyMpiOwSSrTV6S6biLurahKuLjvH1Y)
     * Video: *coming soon*
1.  [Keys to Winning panel discussion](https://drive.google.com/open?id=1V1lDVIJyX1mMin2Hm7AaExps0WAgczi6vAB6UWFuR8c)
     * Video: *coming soon*
1.  [Sales Kick Off Awards Ceremony](https://docs.google.com/presentation/d/1deR4D2GplTGan1E2ENZbhZ0rt4YiUzZEJGX7RLj83yY/edit?usp=sharing)

**Day 2 Sessions**
1.  [Articulating and Selling the GitLab Vision & Customer Journey](https://drive.google.com/open?id=14kO1iTqSwuvV-7CDHThslUv8_QP_EnaZg4yG-1romZc)
     * Video: *coming soon*
1.  [Getting Into Accounts That Say They Don’t Have a Problem](https://drive.google.com/open?id=1dRdKs7BkbyTzTfFZ4JwwjJUIwgTSSOjkG-DEGDGgN30)
     * Video: N/A (not recorded)
1.  [Proactively Competing Against Microsoft](https://drive.google.com/open?id=1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8)
     * Video: *coming soon*
1.  [Customer Expectations and Storytelling](https://drive.google.com/open?id=1hnYT7ulTPpb7C3V_fKdOU9gajAA1oufzp9EiDYQC-mU)
     * Video: N/A (not recorded)
1. [Finding and Defining the Customer’s Problem](https://drive.google.com/open?id=18NynGuJTEwSUnKkRR4A1fQa2xojPxpO3mlnvb3BmMXY)
     * Video: *coming soon*
1.  [One Lab, One Story: Leveling Up Our Demo Labs](https://drive.google.com/open?id=1x125pYEjAQQX5vDo5u2eanAoVtoz5R2muiaYqXL_xEE)
     * Video: N/A (not recorded)
1.  [Customer Success Plan Workshop](https://drive.google.com/open?id=15Qt-UcfRt9cX-4CV7zMsurojTZg_8Kf-u0dMYL16JXQ)
     * Video: N/A (not recorded)
1.  [Gaining Access and Pitching to the Economic Buyer](https://drive.google.com/open?id=166GA0LyvQLG6y-9qAuKoJ6iZUxmjpjklLSZ1J9wC6z0)
     * Video: N/A (not recorded)
1.  [Command Plan Excellence](https://drive.google.com/open?id=1Vjn5ICOpwxbZNFRZhvpwZc0REkHMeEKNSakpFV04EuM)
     * Video: *coming soon*
1.  [Technically Competing Against MSFT in CI Use Case](https://drive.google.com/open?id=1Rfsk5_h5O6DF14wjgmd7WrzQPCC1yGzYPPdYG_JVApg)
     * Video: N/A (not recorded)
1.  [Next Level Engagements: Consulting Acumen](https://drive.google.com/open?id=1ahDNo93BpNSRonLj6C3iWKZfeJy1ZfJZosujpzSzo2U)
     * Video: N/A (not recorded)
1.  [Proactively Competing Against Jenkins](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg)
     * Video: N/A (not recorded)
1.  [Account Planning](https://drive.google.com/open?id=1XPhv3zqtR-q5sKgDx1HhnNkaQUSIVTO0pXj2wPdjafs)
     * Video: *coming soon*
1.  [From SCM and CI to Security: Paths to Ultimate](https://drive.google.com/open?id=1Oq8znDkHrgGK5Xe5D23SdiRLt33OIJZ30OWCHNNDV14)
     * Video: N/A (not recorded)
1.  [Next Level Engagements: Consulting Methodology](https://drive.google.com/open?id=1TWg_grUCegOnJK8yCXxq7lRgVCByoJMCd59u3A1QjMo)
     * Video: N/A (not recorded)

[**Sales Kick Off photo album**](https://photos.app.goo.gl/hcvEyzH3wDdccrQw7)
