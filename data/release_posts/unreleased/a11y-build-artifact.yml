features:
  primary:
    - name: "Automated Accessibility scanning of Review Apps"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: true
      documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html'
      image_url: '/images/unreleased/a11y-report.png'
      reporter: jheimbuck_gl
      stage: verify
      categories:
        - 'Accessibility Testing'
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/issues/25566'
      description: |
        Accessibility of web pages and applications is a growing concern for software developers today. Unfortunately, accessibility testing is often an afterthought occurring late in the software development process. It's typically a manual and inconsistent process, if it's thought about at all. Developments teams often lack clear requirements from Product Managers or Product Designers outlining accessibility standards to follow when building an application.

        Starting in GitLab 12.8, users of Review Apps will be able to automatically scan and get a report of accessibility issues in their Review App. This saves development time by bringing the feedback loop of accessibility issues much closer to the developer and designer, who have the most impact on making the application accessible for all users. As a first step, users will be able to download the full report for every Merge Request. We see this as a solid foundation to build off of, for the future of the accessibility testing category. We are looking forward to feedback from customers on our [vision](/direction/verify/accessibility_testing/) for the Accessibility Testing category.
